# Monash NLP&ML Reading Group
---

- Location: Room 207 - Clayton School of IT
- Time: Mondays, 3-4pm

## Schedule

### ============================ 2018 ================================== ###

### 9. GANs  ###
- Fahimeh
- 2018 March 5
- Reading:
     - [ Generative Adversarial Nets ](Papers/GAN.pdf)
     - [ MaskGAN: Better Text Generation via Filling in the _________ ](Papers/Mask GAN.pdf)
     
### 8. Evolution of Historical Text  ###
- Diptesh
- 2018 February 26

### 7. No Reading Group  ###

### 6. Dual Supervised Learning  ###
- Ming
- 2018 February 12
- Reading:
     - [ Dual Supervised Learning ](Papers/1707.00415.pdf)
     
### 5. Training Neural Networks Without Gradients: A Scalable ADMM Approach  ###
- Najam
- 2018 February 5
- Reading:
     - [ Training Neural Networks Without Gradients: A Scalable ADMM Approach ](Papers/1605.02026.pdf)
     
### 4. Evaluating Discourse Phenomena in Neural Machine Translation  ###
- Sameen
- 2018 January 29
- Reading:
     - [ Evaluating Discourse Phenomena in Neural Machine Translation ](Papers/1711.00513.pdf)

### 3. Deep Compositional Question Answering with Neural Module Networks  ###
- Narjes
- 2018 January 22
- Reading:
     - [ Deep Compositional Question Answering with Neural Module Networks ](Papers/1511.02799.pdf)

### 2. Reporting Score Distributions Makes a Difference: Performance Study of LSTM-networks for Sequence Tagging  ###
- Daniel
- 2018 January 15
- Reading:
     - [ Reporting Score Distributions Makes a Difference: Performance Study of LSTM-networks for Sequence Tagging ](Papers/D17-1035.pdf)
     
### 1. Adversarial Examples for Evaluating Reading Comprehension Systems  ###
- Reza
- 2018 January 8
- Reading:
     - [ Adversarial Examples for Evaluating Reading Comprehension Systems ](Papers/1707.07328.pdf)
     
### ============================ 2017 ================================== ###

### 33. Dialogue Systems  ###
- 2017 December 11
- Quan (Practice talk for Mid-Candidature)
- Najam (continued tutorial from last week)
     - http://www.cs.toronto.edu/~slwang/primal-dual.pdf

### 32. Primal-Dual Algorithm  ###
- Najam
- 2017 December 4
- Venue: Room 145 
- Reading:
     - http://www.cs.toronto.edu/~slwang/primal-dual.pdf

### 31. A Neural Framework for Generalized Topic Models  ###
- Ethan
- 2017 November 27
- Reading:
     - [ A Neural Framework for Generalized Topic Models ](Papers/1705.09296.pdf)
     
### 30. A Day Long Tutorial on Variational Inference and Deep Generative Models ###
- Philip Schulz (PhD visitor from University of Amsterdam)
- 2017 November 16 
- Room 145
- Sessions:
    - Basics of Variational Inference: 10:00am-11:30am
    - Deep Generative Models: 2:30pm-4:00pm
- Reading:
     - [ Tutorial Github Page ](https://github.com/philschulz/VITutorial)

### 29. Early Visual Concept Learning with Unsupervised Deep Learning ###
- Bo
- 2017 November 6
- Reading:
     - [ Early Visual Concept Learning with Unsupervised Deep Learning ](Papers/1234.pdf)

### 28. Statistical Models for Inferring Tumour Heterogeneity ###
- Shams
- 2017 October 30
- Pre-submission Practice Talk
    
### 27. Survey on Neural Approach to Automatic Post Editing Task  ###
- Trang
- 2017 October 23
     
### 26.  Generative and Discriminative Text Classification with RNN  ###
- Ming
- 2017 October 16
- Reading:
     - [ Generative and Discriminative Text Classification with Recurrent Neural Networks ](Papers/1703.01898.pdf)

### 25. Growing a Neural Network for Multiple NLP Tasks  ###
- Poorya
- 2017 October 9
- Reading:
     - [ A Joint Many-Task Model: Growing a Neural Network for Multiple NLP Tasks ](Papers/1611.01587.pdf)

### 24. 2 talks (NER with LSTM) and (Context Dependent RNN for Dialog Systems) ###
- Quan
- 2017 October 2
- Readings:
     - [ Named Entity Recognition with Stack Residual LSTM and Trainable Bias Decoding ](Papers/1706.07598.pdf)
     - Context Dependent Additive Recurrent Neural Network for Dialog Systems

### 23. Reinforcement Learning for Bandit Neural Machine Translation with Simulated Human Feedback  ###
- Daniel
- 2017 September 25
- Reading:
     - [ Reinforcement Learning for Bandit Neural Machine Translation with Simulated Human Feedback ](Papers/D17-1153.pdf)

### 22. Incorporating Context in Neural Machine Translation  ###
- Sameen
- 2017 September 18
- Readings:
     - [ Context Gates for Neural Machine Translation ](Papers/Q17-1007)
     - [ Exploiting Cross-Sentence Context for Neural Machine Translation ](Papers/1704.04347.pdf)
     
### 21. End-to-end optimization of goal-driven and visually grounded dialogue systems  ###
- Narjes
- 2017 September 11
- Reading:
     - [ End-to-end optimization of goal-driven and visually grounded dialogue systems ](Papers/1703.05423.pdf)

### 20. Embedding Senses via Dictionary Bootstrapping  ###
- Ehsan
- 2017 September 4
- Reading:
     - [ Embedding Senses via Dictionary Bootstrapping ](Papers/135.pdf)

### 19. Towards Decoding as Continuous Optimisation in Neural Machine Translation  ###
- Vu (Practice Talk for EMNLP)
- 2017 August 28
- Reading:
     - [ Towards Decoding as Continuous Optimisation in Neural Machine Translation ](Papers/1701.02854.pdf)

### 18. Compressed Nonparametric Language Modelling ###
- Ehsan (Practice Talk for IJCAI)
- 2017 August 21
- Reading:
     - [ Compressed Nonparametric Language Modelling ](Papers/0376.pdf)
     
### 17. Continuous Revision of Combinatorial Structures ###
- Poorya
- 2017 July 31
- Reading:
     - [ Sequence to Better Sequence: Continuous Revision of Combinatorial Structures ](Papers/mueller17a.pdf)

### 16.  Neural Discourse Structure for Text Categorization ###
- Ming
- 2017 July 24
- Reading:
     - [  Neural Discourse Structure for Text Categorization ](Papers/1702.01829.pdf)

### 15. Leveraging Node Attributes for Incomplete Relational Data ###
- Ethan (Practice Talk for ICML)
- 2017 July 17
- Reading:
     - [ Leveraging Node Attributes for Incomplete Relational Data ](Papers/1706.04289.pdf)

### 14. A Statistically Scalable Method for Exploratory Analysis of High-Dimensional Data ###
- Shams
- 2017 July 3
- Reading:
     - [ Under Submission in ICDM](Papers/TBD.pdf)

### 13. Neighborhood Contrast Clustering ###
- Bo
- 2017 June 26
- Reading:
     - [ Under Submission in ICDM](Papers/TBD.pdf)

### 12. Neural Transduction of Complex Discrete Structures for Machine Translation ###
- Poorya
- 2017 June 19
- Confirmation Seminar Practice Talk

### 11. Context in Neural Machine Translation ###
- Sameen
- 2017 May 29
- Readings:
     - [ Exploiting Cross-Sentence Context for Neural Machine Translation ](Papers/1704.04347.pdf)
     - [ Does Neural Machine Translation Benefit from Larger Context? ](Papers/1704.05135.pdf)

### 10. BlackOut: Speeding up Recurrent Neural Network & Tree-to-Sequence Attentional MT ###
- Poorya
- 2017 May 22
- Readings:
     - [ BlackOut: Speeding up Recurrent Neural Network Language Models With Very Large Vocabularies ](Papers/1511.06909.pdf)
     - [ Tree-to-Sequence Attentional Neural Machine Translation ](Papers/1603.06075.pdf)
     - [ Complementary Sum Sampling for Likelihood Approximation in Large Scale Classification ](Papers/AISTATS2017.pdf)

### 9. Aspect-augmented Adversarial Networks for Domain Adaptation ###
- Ming
- 2017 May 15
- Readings:
     - [ Aspect-augmented Adversarial Networks for Domain Adaptation ](Papers/1701.00188.pdf)
     - [ Domain-Adversarial Training of Neural Networks ](Papers/1505.07818.pdf)

### 8. Online Segment to Segment Neural Transduction ###
- Daniel
- 2017 May 8
- Reading:
     - [ Online Segment to Segment Neural Transduction ](Papers/D16-1138.pdf)


### 7. Scalable Bayesian Learning of Recurrent Neural Networks for Language Modeling ###
- Ehsan
- 2017 May 1
- Reading:
     - [ Scalable Bayesian Learning of Recurrent Neural Networks for Language Modeling ](Papers/1611.08034v1.pdf)


### 6. Graph Convolutional Encoders for Syntax-aware Neural Machine Translation ###
- Reza
- 2017 April 24
- Reading:
     - [ Graph Convolutional Encoders for Syntax-aware Neural Machine Translation](Papers/Bastings2017.pdf)

### 5. Trainable Greedy Decoding for Neural Machine Translation ###
- Quan
- 2017 March 13
- Reading:
     - [Trainable Greedy Decoding for Neural Machine Translation](Papers/1702.02429.pdf) 

### 4.  Deep Forest: Towards An Alternative to Deep Neural Networks ###
- Bo
- 2017 March 06
- Reading:
     - [Deep Forest: Towards An Alternative to Deep Neural Networks](Papers/zhou2017.pdf) 


### 3. Probabilistic Graphical Models ###
- Reza
- 2017 February 24
- Reading:
     - [Chapter 8 of PRML](Papers/Bishop-PRML-sample.pdf)
     - [Slides 1](Papers/chapter8a_slides.pdf) and [Slides 2](Papers/chapter8b_slides.pdf)

### 2. Probabilistic Graphical Models ###
- Reza
- 2017 February 10
- Reading:
     - [Chapter 8 of PRML](Papers/Bishop-PRML-sample.pdf) 
     - [Slides 1](Papers/chapter8a_slides.pdf) and [Slides 2](Papers/chapter8b_slides.pdf)

### 1. Compressed Nonparametric Language Modelling ###
- Ehsan
- 2017 February 03
- Reading:
     - [Compressed Nonparametric Language Modelling](Papers/TBD) 

### ============================ 2016 ================================== ###

### 16. Relaxed Optimization Framework for Decoding Neural Translation Models ###
- Vu
- 2016 December 16
- Reading:
     - [Towards A Flexible Relaxed Optimization Framework for Decoding Neural Translation Models](Papers/1701.02854.pdf) 

### 15. Neural Models for Structured Prediction ###
- Sameen
- 2016 December 09
- Confirmation Seminar Practice Talk

### 14. Speed-Constrained Tuning for Statistical Machine Translation Using Bayesian Optimization###
- Daniel
- 2016 November 25
- Reading:
     - [Speed-Constrained Tuning for Statistical Machine Translation Using Bayesian Optimization](Papers/DanielNAACL2016.pdf) 

### 13. Persian-Spanish Machine Translation through a Pivot Language ###
- Benyamin
- 2016 November 18
- Reading:
     - [Persian-Spanish Low-Resource Statistical Machine Translation through English as Pivot Language](Papers/Benyamin.pdf) 

### 12. Neural Network with Dynamic External Memory ###
- Quan
- 2016 November 11
- Reading:
     - [Hybrid Computing Using a Neural Network with Dynamic External Memory](Papers/TBD) 


### 11.  Risk Minimization of Graphical Model Parameters ###
- Sameen
- 2016 October 28
- Reading:
     - [Empirical Risk Minimization of Graphical Model Parameters Given
Approximate Inference, Decoding, and Model Structure](Papers/stoyanov11a.pdf)

### 10. Infinite-order Language Modelling with Compressed Suffix Trees ###
- Ehsan (Practice Talk for TACL/EMNLP)
- 2016 October 14
- Reading:
     - [Fast, Small and Exact: Infinite-order Language Modelling with Compressed Suffix Trees](Papers/TACL2016.pdf)

### 9. Inference of Shallow-transfer Machine Translation Rules ###
- Parya
- 2016 October 07
- Reading:
     - [A generalised alignment template formalism and its application to the inference of shallow-transfer machine translation rules from scarce bilingual corpora](Papers/Parya.pdf) 

### 8. Learning Deep Structured Models ###
- Reza
- 2016 September 02
- Reading:
     - [Learning Deep Structured Models](Papers/chenb15.pdf)

### 7. Inner Attention based Recurrent Neural Networks for Answer Selection ###
- Quan
- 2016 August 25
- Reading:
     - [Inner Attention based Recurrent Neural Networks for Answer Selection](Papers/P16-1122.pdf)

### 6.Globally Normalized Transition-Based Neural Networks ###
- Sameen
- 2016 August 17
- Reading:
     - [Globally Normalized Transition-Based Neural Networks](Papers/1603.06042v2.pdf)

### 5. Variational inference for Monte Carlo objectives ###
- Ehsan
- 2016 August 12
- Reading:
     - [Variational inference for Monte Carlo objectives](Papers/1602.06725.pdf)

### 4. Neural Variational Inference and Learning in Belief Networks ###
- Ehsan
- 2016 August 05
- Reading:
     - [Neural Variational Inference and Learning in Belief Networks](Papers/1402.0030.pdf)

### 3. Ask Me Anything: Dynamic Memory Networks for Natural Language Processing ###
- Sameen
- 2016 July 29
- Reading:
     - [Ask Me Anything: Dynamic Memory Networks for Natural Language Processing](Papers/1506.07285v5.pdf)

### 2. Partition Functions ###
- Ehsan
- 2016 May 27
- Reading(s):
     - [Quick Training of Probabilistic Neural Nets by Importance Sampling](Papers/BengioAISTAT2003.pdf) 
     - [A fast and simple algorithm for training neural probabilistic Language Models](Papers/MnihICML2012.pdf)

### 1. Training Products of Experts by Minimizing the Contrastive Divergence ###
- Ehsan
- 2016 May 20
- Reading:
     - [Training Products of Experts by Minimizing the Contrastive Divergence](Papers/HintonNeuralComputation2002.pdf)
